<?php

namespace App\Controller;

use App\Entity\User;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Serializer\SerializerInterface;

class UserController extends AbstractController
{
    /**
     * @Route("/api/users", name="users", methods={"GET"})
     */
    public function index(UserRepository $userRepository): Response
    {
        $users = $userRepository->findAll();
        return $this->json($users, 200, [], ['groups' => 'user']);
    }

    /**
     * @Route("/api/infos/user", name="index_user", methods={"GET"})
     * @Route("/api/show/user/{id}", name="show_user", methods={"GET"})
     */
    public function show(User $user = null, UserInterface $currentUser): Response
    {
        if (!$user) {
            $user = $currentUser;
        }
        $isAdmin = in_array("ROLE_ADMIN", $currentUser->getRoles(), true);
        if ($currentUser->getUserIdentifier() == $user->getUserIdentifier() || $isAdmin) {
            return $this->json($user, 200, [], ['groups' => 'user']);
        } else {
            $msg = "Forbidden it's not your account";
            return $this->json($msg, 200, []);
        }
    }

    /**
     * @Route("/api/update/user/{id}", name="update_user", methods={"PATCH"})
     */
    public function update(
        User $user,
        Request $request,
        EntityManagerInterface $manager,
        SerializerInterface $serializer,
        UserInterface $currentUser
    ): Response {
        $isAdmin = in_array("ROLE_ADMIN", $currentUser->getRoles(), true);
        if ($user->getUserIdentifier() == $currentUser->getUserIdentifier() || $isAdmin) {

            $data = $request->getContent();
            $userUpdated = $serializer->deserialize($data, User::class, 'json');

            $user->setLastname($userUpdated->getLastname());
            $user->setFirstname($userUpdated->getFirstname());
            $user->setUsername($userUpdated->getUsername());
            $user->setEmail($userUpdated->getEmail());
            $user->setAddress($userUpdated->getAddress());
            $user->setPhoneNumber($userUpdated->getPhoneNumber());

            $manager->persist($user);
            $manager->flush();

            return $this->json($user, 200, [], ['groups' => 'user']);
        } else {
            $msg = "Forbidden, it's not your account";
            return $this->json($msg, 403);
        }
    }

    /**
     * @Route("/api/delete/user/{id}", name="delete_user", methods={"DELETE"})
     */
    public function delete(User $user, EntityManagerInterface $manager, UserInterface $currentUser): Response
    {
        $isAdmin = in_array("ROLE_ADMIN", $currentUser->getRoles(), true);

        if ($user->getUserIdentifier() == $currentUser->getUserIdentifier() || $isAdmin) {
            $manager->remove($user);
            $manager->flush();
            $msg = "User deleted";
            return $this->json($msg, 202, []);
        } else {
            $msg = "Forbidden, it's not your account";
            return $this->json($msg, 403);
        }
    }

    /**
     * @Route("/api/register", name="register", methods={"POST"})
     */
    public function register(
        Request $request,
        SerializerInterface $serializer,
        UserPasswordHasherInterface $hasher,
        EntityManagerInterface $manager
    ): Response {
        $data = $request->getContent();
        $user = $serializer->deserialize($data, User::class, 'json');

        $hashedPassword = $hasher->hashPassword($user, $user->getPassword());
        $user->setPassword($hashedPassword);

        $manager->persist($user);
        $manager->flush();
        return $this->json($user, 200, [], ['groups' => 'user']);
    }

    /**
     * Route("/login", name="login")
     */
    public function login()
    {
        return $this->render('user/index.html.twig');
    }

    /**
     * @Route("/logout", name="logout")
     */
    public function logout()
    {
    }
}
