<?php

namespace App\Controller;

use App\Entity\Game;
use App\Entity\User;
use App\Repository\GameRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\String\Slugger\SluggerInterface;

class GameController extends AbstractController
{
    /**
     * @Route("/", name="game", methods={"GET"})
     */
    public function home(GameRepository $gameRepository): Response
    {
        $games = $gameRepository->findAll();

        return $this->json($games, 200, [], ['groups' => 'game']);
    }


    /**
     * @Route("/games", name="games", methods={"GET"})
     */
    public function index(GameRepository $gameRepository): Response
    {
        $games = $gameRepository->findAll();

        return $this->json($games, 200, [], ['groups' => 'game']);
    }

    /**
     * @Route("/show/game/{id}", name="show_game", methods={"GET"})
     */
    public function show(Game $game): Response
    {
        return $this->json($game, 200, [], ['groups' => 'game']);
    }

    /**
     * @Route("/api/games/user", name="games_by_user", methods={"GET"})
     * @param UserInterface $currentUser
     * @return Response
     */
    public function games(UserInterface $currentUser, User $user): Response
    {
        $games = $user->getGames();

        return $this->json($games, 200, [], ['groups' => 'game']);
    }

    /**
     * @Route("/api/games/count/user/{id}", name="games_by_user_id", methods={"GET"})
     * @param User $user
     * @return Response
     */
    public function gamesByUser(User $user): Response
    {
        $games = $user->getGames();

        return $this->json($games, 200, [], ['groups' => 'game']);
    }

    /**
     * @Route("/api/create/game", name="new_game", methods={"POST"})
     * @param Request $request
     * @param EntityManagerInterface $manager
     * @param SerializerInterface $serializer
     * @param User $user
     * @return Response
     */
    public function create(
        Request $request,
        EntityManagerInterface $manager,
        SerializerInterface $serializer,
        User $user
    ): Response {
        $data = $request->getContent();
        $game = $serializer->deserialize($data, Game::class, 'json');
        $game->setUser($user);
        $manager->persist($game);
        $manager->flush();
        return $this->json($game, 201, [], ['groups' => 'game']);
    }


    /**
     * @Route("/api/update/game/{id}", name="update_game", methods={"PATCH"})
     * @param Game $game
     * @param Request $request
     * @param EntityManagerInterface $manager
     * @param SerializerInterface $serializer
     * @return Response
     */
    public function update(
        Game $game,
        Request $request,
        EntityManagerInterface $manager,
        SerializerInterface $serializer
    ): Response {
        $data = $request->getContent();
        $gameUpdated = $serializer->deserialize($data, Game::class, 'json');

        $game->setName($gameUpdated->getName());
        $game->setDescription($gameUpdated->getDescription());
        $game->setPrice($gameUpdated->getPrice());
        $game->setName($gameUpdated->getName());
        $game->setPlatform($gameUpdated->getPlatform());
        $game->setRating($gameUpdated->getRating());
        $game->setEditor($gameUpdated->getEditor());
        $game->setType($gameUpdated->getType());
        $game->setReleasedDate($gameUpdated->getReleasedDate());

        $manager->persist($game);
        $manager->flush();
        return $this->json($game, 200, [], ['groups' => 'game']);
    }

    /**
     * @Route("/filter/games", name="filter_games")
     */
    // public function filterGames(Request $request, GameRepository $gameRepository): Response
    // {
    //     $data = $request->toArray();
    //     if ($data['type'] == 'Genre') {
    //         unset($data['type']);
    //     }
    //     if ($data['platform'] == 'Plateforme') {
    //         unset($data['platform']);
    //     }
    //     $games = $gameRepository->findBy($data);
    //     return $this->json($games, 200, [], ['groups' => 'game']);
    // }

    /**
     * @Route("/api/delete/game/{id}", name="delete_game", methods={"DELETE"})
     */
    public function delete(Game $game, EntityManagerInterface $manager): Response
    {

        $manager->remove($game);
        $manager->flush();

        $msg = ["delete_game" => "Success well deleted"];
        return $this->json($msg, 204, [], ['groups' => 'game']);
    }

    /**
     * @Route("/api/image/{id}", name="game_image")
     */
    public function addImage(Game $game, Request $request, EntityManagerInterface $manager): Response
    {
        $image = $request->files->get('file');
        $originalName = pathinfo($image->getClientOriginalName(), PATHINFO_FILENAME);
        $newName = $originalName . '.' . uniqid() . '.' . $image->guessExtension();
        $image->move($this->getParameter('image'), $newName);

        $game->setImage($newName);
        $manager->persist($game);
        $manager->flush();

        return $this->json($game, 200, [], ['groups' => 'game']);
    }
}
