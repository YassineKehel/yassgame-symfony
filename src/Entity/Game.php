<?php

namespace App\Entity;

use App\Repository\GameRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass=GameRepository::class)
 */
class Game
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"game", "user"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"game", "user"})
     */
    private $name;

    /**
     * @ORM\Column(type="text")
     * @Groups({"game"})
     */
    private $description;

    /**
     * @ORM\Column(type="integer")
     * @Groups({"game"})
     */
    private $price;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"game"})
     */
    private $rating;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"game"})
     */
    private $editor;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"game"})
     */
    private $type;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"game"})
     */
    private $releasedDate;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="games")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"game"})
     */
    private $user;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"game"})
     */
    private $platform;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"game"})
     */
    private $image;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getPrice(): ?int
    {
        return $this->price;
    }

    public function setPrice(int $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getRating(): ?string
    {
        return $this->rating;
    }

    public function setRating(string $rating): self
    {
        $this->rating = $rating;

        return $this;
    }

    public function getEditor(): ?string
    {
        return $this->editor;
    }

    public function setEditor(string $editor): self
    {
        $this->editor = $editor;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getReleasedDate(): ?string
    {
        return $this->releasedDate;
    }

    public function setReleasedDate(string $releasedDate): self
    {
        $this->releasedDate = $releasedDate;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getPlatform(): ?string
    {
        return $this->platform;
    }

    public function setPlatform(string $platform): self
    {
        $this->platform = $platform;

        return $this;
    }

    public function getImage(): ?string
    {
        return $this->image;
    }

    public function setImage(string $image): self
    {
        $this->image = $image;

        return $this;
    }
}
